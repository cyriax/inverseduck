dbg = False

import random
import string
import re

split=lambda string, char: re.split(r'(?<!\\)'+char, string)
def _strip(string):
    if not string: return string
    while string[0] in " \t\n":
        string = string[1:]
        if not string: return string
    while string[-1] in " \t\n":
        string = string[:-1]
        if not string: return string
    return string

what = {}
class Choice:
    def __init__(self, items, name = ""):
        #items = items.split(",")
        items = split(items,",")
        self.items = []
        self.weights = []
        self.name = name
        for item in items:
            weight = 1
            if "!" in item:
                item,*weights = split(item,"!")
                if len(weights) > 0:
                    if len(weights) > 1:
                        ValueError("One can only use one weight per choice")
                    weight = float(weights[0])
            item = _strip(item)
            item = item.replace("\\,",",")
            item = item.replace("\\:",":")
            item = item.replace("\\!","!")
            item = item.replace("\\\\","\\")
            self.items.append(item)
            self.weights.append(weight)
        self.items = list(enumerate(self.items))
    def choose(self):
        index, choiceform = random.choices(self.items,self.weights)[0]
        self.weights[index] *= 0.1
        choice = ""
        for stringpart,category,_,_ in string.Formatter.parse(None, choiceform):
            choice += stringpart
            if category is None: continue
            if category not in what:
                print("Could not find {"+category+"}")
                choice += "{"+category+"}"
            else:
                try:
                    choice += what[category].choose()
                except RecursionError:
                    print("recusion depth at:",self.name)
        if dbg: print(self.name,"->",choiceform,"=>\n\t",choice)
        return choice
        
def longparser(definitions):
    category, theseitems = None, ""
    for line in split(definitions,"\n"):
        if not line: continue
        line = _strip(line)
        if line.startswith("#"): continue
        if ":" in line:
            if category is not None and theseitems:
                what[category] = Choice(theseitems, category)
            category, items = split(line,":")
            theseitems = items
        else:
            theseitems += line
    what[category] = Choice(theseitems, category)

def postProcess(excl):
    s = excl.split(".")
    sentenceparticles = []
    for sentence in excl.split("."):
        if not sentence.startswith(" "):
            if len(sentence) > 1:
                c = sentence[0]
                sentence = c.upper() + sentence[1:]
            sentenceparticles.append(sentence)
        elif len(sentence) > 2:
            c = sentence[1]
            sentence = " "+c.upper() + sentence[2:]
            sentenceparticles.append(sentence)
    excl = ".".join(sentenceparticles)
    excl = excl.replace("in dem","im")
    excl = excl.replace("zu dem","zum")
    excl = excl.replace("zu der","zur")
    excl = excl.replace("  "," ")
    excl = excl.replace("  "," ")
    excl = excl.replace("  "," ")
    return excl
    

definitions = ""
with open("definitions_de.txt",'r') as f:
    definitions = f.read()
longparser(definitions)
exclamation = what['final'].choose()
exclamation = postProcess(exclamation)
print(exclamation)

