# use  ':source vimrc' for vim syntax highlighting
# syntax description:
# 'label:' - a label followed by a colon followed by a list of choices
# '{label}' - reference to a label that will be replaced
# 'string' - will be printed
# ',' - delimiter between choices
# '!weight' - weight of an option. Must be a number. 0 to switch off.
# '#' - comment
# newlines and whitespace before and after choices will be removed.
# Escaping , : ! \ with \ now should work

####
# Common Section
# Please stay in your space to avoid merge conflicts.
# New members please add your space below this line:

# Cyriax
# names don't need articles, but may have them
name_m: Dave, Tobi, Cyriax, Flauschmaster, Trump, Putin, Bill Gates, Marc Zuckerberg, Zuckerberg, Elon Musk, Kevin
name_f: Nele, Hackhörnchen, Angela Merkel, Uschi, Chantale, Melanie
name_z: {nick}
name_n: {nick}
name_p: Aliens, Russen, 4chan User, Flatearther, Boomer,
    Pferde, Zigaretten, Ausserirdische, Kiffel, Studenten,
    Professoren, Informatiker, Haecksen, Enten, Computer, "Lebenskuenstler",
    Freudenhaeuser, Banker, Illuminaten, Freimaurer
subject_f: {name_f}, {name_z}!0.1, die {name_f}!0.2, die {name_z}!0.1
subject_m: {name_m}, {name_z}!0.1, der {name_m}!0.2, der {name_z}!0.1, niemand!0.2
subject_z: {name_z}
subject_n: {name_n}, das {name_n}
subject_p: {name_p}, {subject_article_p} {name_p}, alle!0.2
subject_article_m: der, mein, dein, ein
subject_article_f: die, meine, deine, eine
subject_article_n: das, mein, dein, ein
subject_article_p: die, meine, deine, einige, alle, fast keine
subject_person: {subject_m},{subject_f}
subject_group: {subject_p}
subject: {subject_person}, {subject_group}

object_name_m: Macker, Mann, Hund, Kaffee, Computer, KGB, Vorsitzenden der SPD, Montagmorgen, deutschen Pfadfinderbund, Fernseher, Flughafen, Graben, Raumteiler
object_name_f: Frau, Chefin, Friseuse, Chickse, NSA, ESA, NASA, ISS, katholische Kirche, WHO, Mauer, Wand, Microwelle
object_name_n: Fenster, Boot, Pferd, Internet, Telefonnetz, Tanzverbot, Grundrecht, Security-System, Mittagsfernsehen, Fernsehen
object_name_p: Aliens, Pferde, Zigaretten, Ausserirdischen, Kiffel, Studenten, Professoren, Informatiker, Haecksen, Enten, Computer, "Lebenskuenstler", Freudenhaeuser, Banker, Illuminaten, Freimaurer
object_name_group_f: NSA, NASA, 
object_article_m: den, meinen, deinen, einen
object_article_f: die, meine, deine, eine
object_article_n: das, mein, dein, ein
object_article_p: die, meine, deine, einige, alle, fast keine
object_m: {object_article_m} {object_name_m}
object_f: {object_article_f} {object_name_f}
object_n: {object_article_n} {object_name_n}
object_p: {object_article_p} {object_name_p}
object_fm: ihren {object_name_m},{object_m}
object_ff: ihre {object_name_f}, {object_f}
object_fn: ihr {object_name_n}, {object_n}
object_fp: ihre {object_name_p},{object_p}
object_mm: seinen {object_name_m},{object_m}
object_mf: seine {object_name_f}, {object_f}
object_mn: sein {object_name_n},{object_n}
object_mp: seine {object_name_p}, {object_p}
object_nm: seinen {object_name_m}, {object_m}
object_nf: seine {object_name_f},{object_f}
object_nn: sein {object_name_n},{object_n}
object_np: seine {object_name_n},{object_p}
object_pm: ihren {object_name_m}, {object_m}
object_pf: ihre {object_name_f},{object_f}
object_pn: ihre {object_name_n},{object_n}
object_pp: ihre {object_name_p},{object_p}
object_group:
object_person:
object_thing:
object: {object_f},{object_m},{object_n},{object_p}
indirect_object_article_m: dem, meinem, deinem, einem
indirect_object_article_f: der, meiner, deiner, einer
indirect_object_article_n: dem, meinem, deinem, einem
indirect_object_article_p: den, meinen, deinen, einigen, allen, fast keinen
indirect_object_m: {indirect_object_article_m} {object_name_m}
indirect_object_f: {indirect_object_article_f} {object_name_f}
indirect_object_n: {indirect_object_article_n} {object_name_n}
indirect_object_p: {indirect_object_article_p} {object_name_p}
indirect_object_fm: ihrem {object_name_m},{indirect_object_m}
indirect_object_ff: ihrer {object_name_f}, {indirect_object_f}
indirect_object_fn: ihrem {object_name_n}, {indirect_object_n}
indirect_object_fp: ihren {object_name_p},{indirect_object_p}
indirect_object_mm: seinem {object_name_m},{indirect_object_m}
indirect_object_mf: seiner {object_name_f}, {indirect_object_f}
indirect_object_mn: seinem {object_name_n},{indirect_object_n}
indirect_object_mp: seinen {object_name_p}, {indirect_object_p}
indirect_object_nm: seinen {object_name_m}, {indirect_object_m}
indirect_object_nf: seiner {object_name_f},{indirect_object_f}
indirect_object_nn: seinen {object_name_n},{indirect_object_n}
indirect_object_np: seinen {object_name_n},{indirect_object_p}
indirect_object_pm: ihrem {object_name_m}, {indirect_object_m}
indirect_object_pf: ihrer {object_name_f},{indirect_object_f}
indirect_object_pn: ihrem {object_name_n},{indirect_object_n}
indirect_object_pp: ihren {object_name_p},{indirect_object_p}
indirect_object: {indirect_object_f},{indirect_object_m},{indirect_object_n},{indirect_object_p}

verb_zu: bauen, errichten, vernichten, betrachten, verwoehnen
verb_singular: liebt, mag, steht auf, betrachtet, schlaegt, denkt an
verb_plural: lieben, moegen, stehen auf, betrachten, schlagen, denken an
verb_phrase_singular:
    {verb_singular} {object},
    gibt {object} {indirect_object},
    sieht {object} mit {indirect_object},
    {verb_singular} {object}\, aber {verb_singular} {object} umso mehr
    
verb_phrase_plural:
    {verb_plural} {object},
    geben {object} {indirect_object},
    sehen {object} mit {indirect_object},
    {verb_plural} {object}\, aber {verb_plural} {object} umso mehr
    
    


# Cyriax nick generator
nick_noun: Flausch, 1337, Hacker, Shadow, Death, Noun, Gay, Furry, Uncle, Daddy, Cuddle, Hack
nick_verb: master, god, lord, l0rd, h4xx0r, hunter, assasin, eater, walker, verber, spinner, lover, dog, healer, friend, bear, horse, hörnchen
nick_adjective: adjective, true, mighty, red, foolish, wise, awesome, shaggy, foolish
nick_realm: von WoW, vom LARP, aus dem RL, aus dem Bus, aus der Uni, aus den DiabloII Tagen, aus der Pen und Paper Runde, aus dem IRC, vom dem einen Geburtstag, von der Party
nick_n1: 9,0,8,
nick_n2: 2,1,3,4
nick_number: 69,xxx,{nick_n1}{nick_n2},!6
nick:
    {nick_noun}{nick_verb},
    {nick_noun}{nick_verb}{nick_number},
    "{nick_noun}{nick_verb} the {nick_adjective}",
    {nick_noun}{nick_verb}{nick_number} {nick_realm},
    "{nick_noun}{nick_verb} the {nick_adjective}" {nick_realm}


# Hackhoernchen:

exclamation_random: Uff., Aaaaaaaaaaaah., ..., Yay., Gnah., Puuuuuuh., Orr ..., Mimimi., Warum muss das denn ... Gottverdammt., Woot., Haha., Hihi., Damn., Uargh., Yay., Hach ..., Endlich., Sorry., 'Tschuldigung.
exclamation_lazy: Lass mal Pause machen., Raucherpause?, {exclamation_random} Raucherpause?, Erstmal Kaffee., {exclamation_random} Ich brauch nen Kaffee., Erstmal Schuhe aus., Lass mal wieder lüften., Ich mach mal Fenster auf\, ja?, Können wir das Fenster wieder zu machen?, Ist schon Zeit für Bier?, Sonntag am Badesee. Das war schön., Lass mal heute eher Schluss machen., Hier guck mal\, voll lustig. *link*, Ja?, Geil\, der Bitcoin ist gestiegen.
exclamation_eww: Pfui. Was riecht denn hier so?, *rüüüüüüülps*, *pups*, Wer's zuerst riecht ist's gewesen., *schmatz*, *schlürf* ... Aaaaaah\!, Ey\, behalt deine Füße bei dir\!, Hihi\, Penis., *kratz*, Hör auf zu treten\!

exclamation: {exclamation_random}!3, {exclamation_lazy}, {exclamation_eww}, {exclamation} {exclamation}!5


blame_target_group: die Kunden, Aliens, Flatearther, die Boomer, 4-Chan-User,
    {blame_target_group} und {blame_target_group}, {blame_target_group} und {blame_target_single}, {blame_target_single} und {blame_target_group}, {blame_target_single} und {blame_target_single}
blame_target_name: Kevin, Melanie, der Neue, die Neue
blame_target_single: {blame_target_name}!4, die Politik, die Zeitumstellung, der Code, das Management, die Produktion, das 2038-Problem
blame: 
    {blame_target_single} hat das verkackt., {blame_target_single} ist Schuld., Das war ich nicht. Frag {blame_target_single}., Kann {blame_target_single} vielleicht mal irgendwas richtig machen?, {blame_target_single} - einfach anzünden., Dafür bekommt {blame_target_single} noch richtig auf die Finger., {blame_target_name} hat die Brotdose vergessen. Da ist {food_maincourse} drin. Wetten wie das Montag riecht?, {blame_target_single} tritt mich unter'm Tisch\!,
    {blame_target_group} haben das verkackt., {blame_target_group} sind Schuld., Das war ich nicht. Frag {blame_target_group}., Können {blame_target_group} vielleicht mal irgendwas richtig machen?, {blame_target_group} - alle anzünden., Dafür bekommen {blame_target_group} noch richtig auf die Finger.,
    {exclamation} {blame}!10, {blame} {exclamation}!10

####
####
# Food Section
food_animal:
    Schweine,Rinder,Lamm,Hühner,Enten,Bock,Puten,Wild,Ratten,Nougatschweinchen,Kolibri,Dackel,Cthulhu,Bodybuilder
food_animalpart:
    nacken, brust, kotelett, hack, schenkel, bein, würstchen, leber, blut, haar
food_meat: Fisch, Fischstäbchen, Hack, Heuschreckenprotein, {food_animal}{food_animalpart}!4,Soja-{food_animal}{food_animalpart},Soja{food_animalpart},{food_animal}{food_animalpart}ersatz
food_veggie: Kohl, Karotten, Erbsen, Linsen, {food_veggie} und {food_veggie}!8,Zwiebel, Lauch, Porree, Grünkohl, Bambus, Paprika
food_carb: Kartoffel, Reis, Kidneybohnen, Nudel, Brot,Fladenbrot, Gemüse
food_mainingredient: {food_meat},{food_veggie},{food_carb}
food_main: {food_mainingredient}{food_form},{food_mainingredient}!0.2, {food_meat},{food_mainingredient} ({food_method}), {food_mainingredient}{food_form} ({food_method})!2, {food_meat} ({food_method})!3,{food_carb} und {food_veggie}!0.5
food_method: überbacken, gebacken, flambiert, püriert, gestampft, geräuchert, paniert,frittiert, tiefgefroren, getrocknet, gefriergetrocknet, luftgetrocknet, gedörrt, eingeweicht, weichgeklopft, ausgekocht, mariniert, gemahlen, eingelegt, mild gesäuert, gesiebt, blutig, sautiert, angebraten, durchgebraten, gedünstet, dramatisiert, blanchiert, operiert, erhitzt, roh, sehr roh, frisch, eingekocht, gefroren, zweifach {food_method}!10
food_form: pizza,auflauf,teller,brot,pfanne,terrine,suppe,gulasch,topf,eintopf
food_protein: Erbsen,Hack,Speck,Pilzen,Käse,Fisch,Fischstäbchen,Huhn,Bohnen
food_saucetype: Pfefferminz, Sahne, Hollandaise, Jaeger, Dunkle Jagd, Pfeffer, Knoblauch, Pilz, Pfirsich, Mandarinen, Bernaise, Joppie
food_addon: {food_saucetype}sauce!5, Käse,Sahne,Algen,Röstzwiebeln,Oliven,Pilze
food_spicetype: mit Pfeffer,indisch,chinesisch,scharf,lasch,"englisch",mit Glutamat, mit Hefeextrakt, salzig,
food_machine: dem Ofen,dem Topf,der Mikrowelle,der Pfanne,dem Reiskocher,dem Thermomix, dem Kühlschrank, dem Wasserkocher,dem Römertopf, dem Konvektomaten, der Tube
food_name_addable: Müllerin, Bergmann, Gärtner, Gärtnerin, Jäger
food_name_single: Hawaii, a la Provence, nach Omas Art, nach Art des Hauses, nach Art der Huren, in Jus, wie bei Muttern
food_name: {food_name_single}!4, "{food_name_addable}"!3, "{food_name_addable}"-Art!3
food_maincourse:
    {food_main} {food_name} mit {food_addon} {food_spicetype} gewürzt,
    {food_main} {food_name} mit {food_addon} {food_spicetype} gewürzt,
    {food_main} mit {food_addon} {food_spicetype} gewürzt,
    {food_main} mit {food_addon} aus {food_machine},
    {food_main} {food_name} mit {food_addon} aus {food_machine},
    {food_main} mit {food_mainingredient} {food_method} aus {food_machine}
food_iceflavour: Vanille, Erdbeer, Schoko, Pistazien, Straciatella, Nuss, Bananen
#food_dessert: Pudding, {food_iceflavour}sauce auf {food_iceflavour}eis!5, {food_iceflavour}eis!5, Milchreis mit {food_iceflavour}sauce!2, Grießklößchen {food_sweet_method} mit {food_topping} und {food_iceflavour}sauce
food_dessert_main: Pudding, {food_iceflavour}eis, Milchreis, Grießklößchen
food_dessert: {food_dessert_main}, {food_dessert_main} mit {food_iceflavour}sauce, {food_dessert_main} {food_sweet_method} mit {food_topping} und {food_iceflavour}sauce
food_topping: heißen Kirschen, Krokant, Streuseln
food_sweet_method: schokoliert, flambiert, gezuckert, im Staubzucker gewälzt, gefrostet, {food_sweet_method} und {food_sweet_method}



food_machine_funny: {food_machine}, dem Thermomix, dem Wasserkocher, dem Kühlschrank

food_funny: {food_maincourse_funny}, {food_maincourse_funny}. Danach gibt es {food_dessert_funny}.
food_tryactually: {food_maincourse}, {food_maincourse}. Danach gibt es {food_dessert}.

food_kantine: Heute gibt's in der Kantine {food_maincourse}., Heute gibt's in der Kantine {food_maincourse}. {food_reaction}
food_lunchbox: Hab Reste von gestern dabei. Gibt {food_maincourse}., Hab Reste von gestern dabei. Gibt {food_maincourse}. {food_reaction}
food_recently: Gestern gab's {food_maincourse}. {food_reaction}, Gestern gab's {food_dessert}. {food_reaction}
food_hungry: Hab jetzt richtig Lust auf {food_maincourse}., Hab jetzt richtig Lust auf {food_dessert} 
food_reaction: Das mag ich besonders., Lecker., Da nehme ich noch Nachschlag., Naja mein Schatz kocht das besser., Schon wieder ..., Buärgh., Boah nee., Ihgitt., Lass mal lieber was bestellen., Da ist bestimmt Pferd drin., Meinst du da sind Eier drin?, Davon bekomme ich immer Pickel., Willst du die Hälfte?, Da sind die Blähungen vorprogrammiert., Na?

food: {food_funny}!0, {food_tryactually}, Essen mit Essensgeschmack und Nahrung dabei!0

food_context: {food_kantine}, {food_lunchbox}, {food_recently}


####
# Computer Section
computer_exclamation: Uff, Uargh, Alter, Ne, Yay, Woot, Belastend, Och Nö, Trololo, LOL, ROFL, Damn, Verdammt, Verflixt, Huston wir haben ein Problem
computer_object: der Compiler, die CPU, der RAM, die Harddisk, der Cache, mein Compiler
computer_time:
    mal wieder, immer, schon wieder, noch immer, normalerweise
computer_quality_qualifier:
    nicht, nicht genug, zu viel, genau so wie man es erwarten würde,
    zu gut, zu oft, nicht oft genug
computer_verb:
    spinnt, brennt, funktioniert, läuft, trollt
computer_verb_phrase: {computer_verb} {computer_verb_quality}
computer_verb_quality: {computer_quality_qualifier}, {computer_time} {computer_quality_qualifier}
computer_predicate: {computer_object} {computer_verb}
computer_predicate_complex:
    {computer_object} {computer_verb_phrase},
    {computer_object} {computer_verb_phrase} {computer_circumstance}
computer_predicate_simple:
computer_circumstance:
    wenn {computer_predicate},
    wenn {computer_predicate} oder {computer_predicate},
    wenn {computer_predicate} und {computer_predicate},
    immer wenn {computer_predicate}
computer_sentence:
    {computer_predicate_complex}.,
    {computer_predicate_complex} {computer_circumstance}
computer:
    {computer_sentence},
    {computer_exclamation}. {computer},
    {computer} {computer_exclamation}.

computer_context: {computer}


####
# Gossip
gossip_name_m: Dave, Tobi, Cyriax, Flauschmaster, Trump, Putin, Bill Gates, Marc Zuckerberg, Zuckerberg, Elon Musk
gossip_name_f: Nele, Hackhörnchen, Angela Merkel, Uschi
gossip_name_n: Flauschmaster
gossip_name_p: Aliens, die Russen

gossip_time_recurring: mal wieder, schon wieder, immer noch, wie immer, wie so oft, warum auch immer, ohne Plan, ohne an die Konsequenzen zu denken, ohne Mama zu fragen
gossip_adjective_base: neue, alte, frische, junge, schoene, angesehene, haessliche, wichtige
gossip_adjective_f: {gossip_adjective_base}, 
gossip_adjective_m: {gossip_adjective_base}n,
gossip_adjective_n: {gossip_adjective_base}s,
gossip_adjective_p: {gossip_adjective_base}n,
    roten, vollen, wichtigen
gossip_adverb: zurecht, verdientermassen, hinterruecks, ueberraschend, typischerweise, voll premium, echt, mit allen Mitteln
gossip_verb1: alleingelassen, stehen gelassen, kaputtgemacht, ignoriert, gehackt, vom Netz genommen, durchgenommen, verletzt, umgangen, verschuettet, verhehlt, verschwinden lassen, total hintergangen, angelogen, verraten, angeklagt
gossip_verb: {gossip_verb1}, {gossip_adverb} {gossip_verb1}!0.2

gossip_object_f:
    {gossip_adjective_f} {gossip_object_f},{gossip_name_f},
    Frau, Chefin, Friseuse, Chickse, NSA, ESA, NASA, ISS, katholische Kirche, WHO
gossip_object_m:
    {gossip_adjective_m} {gossip_object_m}, {gossip_name_m},
    Macker, Mann, Hund, Kaffe, Computer, KGB, Vorsitzenden der SPD, Montagmorgen, deutschen Pfadfinderbund
gossip_object_n:
    {gossip_adjective_n} {gossip_object_n},
    Fenster, Boot, Pferd, Internet, Telefonnetz, Tanzverbot, Grundrecht, Security-System, Mittagsfernsehen, Fernsehen

gossip_object_p1:
    Aliens, Pferde, Zigaretten, Ausserirdischen, Kiffel, Studenten, Professoren, Informatiker, Haecksen, Enten, Computer, "Lebenskuenstler", Freudenhaeuser, Banker, Illuminaten, Freimaurer
gossip_object_p:
    {gossip_adjective_p} {gossip_object_p1}, {gossip_object_p1}

gossip_article_m: den, meinen, deinen, einen
gossip_article_f: die, meine, deine, eine
gossip_article_n: das, mein, dein, ein
gossip_article_p: die, meine, deine, einige, alle, fast keine
gossip_predicate_fm: ihren {gossip_object_m}, {gossip_article_m} {gossip_object_m}
gossip_predicate_ff: ihre {gossip_object_f}, {gossip_article_f} {gossip_object_f}
gossip_predicate_fn: ihr {gossip_object_n}, {gossip_article_n} {gossip_object_n}
gossip_predicate_fp: ihre {gossip_object_p}, {gossip_article_p} {gossip_object_p}
gossip_predicate_mm: seinen {gossip_object_m}, {gossip_article_m} {gossip_object_m}
gossip_predicate_mf: seine {gossip_object_f}, {gossip_article_f} {gossip_object_f}
gossip_predicate_mn: sein {gossip_object_n}, {gossip_article_n} {gossip_object_n}
gossip_predicate_mp: seine {gossip_object_p}, {gossip_article_p} {gossip_object_p}
gossip_predicate_pm: ihren {gossip_object_m}, {gossip_article_m} {gossip_object_m}
gossip_predicate_pf: ihre {gossip_object_f}, {gossip_article_f} {gossip_object_f}
gossip_predicate_pn: ihre {gossip_object_n}, {gossip_article_n} {gossip_object_n}
gossip_predicate_pp: ihre {gossip_object_p}, {gossip_article_p} {gossip_object_p}

gossip_theme:
    {gossip_name_f} hat {gossip_time_recurring} {gossip_predicate_fm} {gossip_verb},
    {gossip_name_f} hat {gossip_time_recurring} {gossip_predicate_ff} {gossip_verb},
    {gossip_name_f} hat {gossip_time_recurring} {gossip_predicate_fn} {gossip_verb},
    {gossip_name_f} hat {gossip_time_recurring} {gossip_predicate_fp} {gossip_verb},
    {gossip_name_m} hat {gossip_time_recurring} {gossip_predicate_mm} {gossip_verb},
    {gossip_name_m} hat {gossip_time_recurring} {gossip_predicate_mf} {gossip_verb},
    {gossip_name_m} hat {gossip_time_recurring} {gossip_predicate_mn} {gossip_verb},
    {gossip_name_m} hat {gossip_time_recurring} {gossip_predicate_mp} {gossip_verb},
    {gossip_name_p} haben {gossip_time_recurring} {gossip_predicate_pm} {gossip_verb},
    {gossip_name_p} haben {gossip_time_recurring} {gossip_predicate_pn} {gossip_verb},
    {gossip_name_p} haben {gossip_time_recurring} {gossip_predicate_pf} {gossip_verb},
    {gossip_name_p} haben {gossip_time_recurring} {gossip_predicate_pp} {gossip_verb}
gossip_preamble:
    Hast du gehört?, Hörma., Also., {exclamation_random}, {exclamation_lazy}
gossip_my_opinion_intro:
    Ich bin der Meinung, Ist doch klar\, dass, Ich meine man weiss doch\, dass,
    Ich weiss\,,
gossip_opinion_predicate:
    das geht so nicht, das darf nicht passieren,
    das sollte so nicht sein, solche Menschen muss man einsperren,
    das ist bestimmt Fake-News, sowas hoert man ja nur in den wirklichen Nachrichten,
    wer an sowas auch nur denkt ist ein Perverser,
    niemand die Absicht hat eine Mauer zu bauen,
gossip_my_opinion:
    {gossip_my_opinion_intro} {gossip_opinion_predicate}
gossip_other_opinion_statement:
    hat gesagt, hat mir gesagt, hat mir letztens gesagt,
    hat letztens darueber geredet und war der Meinung\, dass,
    hat letztens bekanntgegeben\, dass,
    hat ja auf Twitter gepostet und dabei gesagt\, dass,
    ist komplett durch und meint
gossip_other_opinion:
    {gossip_name_m} {gossip_other_opinion_statement} {gossip_opinion_predicate},
    {gossip_name_f} {gossip_other_opinion_statement} {gossip_opinion_predicate},
    {gossip_name_n} {gossip_other_opinion_statement} {gossip_opinion_predicate},
gossip_opinion:
    {gossip_opinion} {gossip_opinion},
    {gossip_my_opinion}.,
    {gossip_other_opinion}.
gossip_context:
    {gossip_theme}. {gossip_opinion},
    {gossip_preamble} {gossip_theme}.!0.2,
    {gossip_preamble} {gossip_theme}. {gossip_opinion}!0.2

###  
# Simple fact generator
simple_verb_zu:
    bauen, errichten, vernichten, betrachten, verwoehnen
simple_verb_singular:
    liebt, mag, steht auf, betrachtet, schlaegt
simple_verb_plural:
    lieben, moegen, stehen auf, betrachten, schlagen, denken an
simple_fact:
    #{subject_person} hat die Absicht {object} zu {simple_verb_zu},
    #{subject_group} haben die Absicht {object} zu {simple_verb_zu},
    {subject_person} {verb_phrase_singular},
    {subject_group} {verb_phrase_plural}
simple_fact_intro_statment:
    Hier\, eine Schlagzeile.,
    Ooh\, schaum mal was ich auf Facebook gefunden habe.,
    Das ist interessant...,
    Hey\, hast du schon gehoert.
simple_fact_context:
    {simple_fact_intro_statment} "{simple_fact}."



final:
    {food_context},
    {computer_context},
    {exclamation},
    {blame},
    {gossip_context},
    {simple_fact_context}
