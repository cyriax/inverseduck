
syn match String '.'
syn match Identifier '^\w*:'
syn match Label '{\w*}' 
syn match Type '\![.0-9]*'
syn match Comment '^\W*#.*$'
syn match HighlightDelim ','
syn match Special '\\.'
hi def HighlightDelim term=bold cterm=bold gui=bold
